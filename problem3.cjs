const getCarsAlphabetically = (cars) => {
    if (cars=== undefined ||cars.length==0||typeof(cars)=='string' || typeof(cars)=='number' ) {
        return []
    }
    else if(!Array.isArray(cars)){
            return cars
    }
    console.log(cars[0].car_model)
    for (let outerIndex = 0; outerIndex < cars.length; outerIndex++) {
        for (let innerIndex = outerIndex + 1; innerIndex < cars.length; innerIndex++) {
            let outerCar = cars[outerIndex].car_model.toLowerCase()
            let innerCar = cars[innerIndex].car_model.toLowerCase()

            if (outerCar > innerCar) {
                [cars[innerIndex], cars[outerIndex]] = [cars[outerIndex], cars[innerIndex]]
            }

        }
    }
    return cars
}
module.exports = getCarsAlphabetically