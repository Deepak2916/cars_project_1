
const getAllYearsOfCars=(cars)=>{
    if (cars=== undefined ||cars.length==0||typeof(cars)=='string' || typeof(cars)=='number' ) {
        return []
    }
    else if(!Array.isArray(cars)){

            return [cars.car_year]
    }
    const allYearsOfCars=[]
    for(let index=0;index<cars.length-1;index++){
        
        allYearsOfCars.push(cars[index].car_year)
    }
    return allYearsOfCars
}

module.exports=getAllYearsOfCars