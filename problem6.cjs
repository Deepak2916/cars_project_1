const getBMWAndAudiCars = (cars) => {
    if (cars=== undefined ||cars.length==0||typeof(cars)=='string' || typeof(cars)=='number' ) {
        return []
    }
    else if(!Array.isArray(cars)){
        if(cars.car_make=='BMW' || cars.car_make=='Audi'){
            return cars
        }
        else{
            return []
        }
    }
    let BMWAndAudiCars = []
    for (let index = 0; index < cars.length - 1; index++) {
        if (cars[index].car_make === 'BMW' || cars[index].car_make === 'Audi') {
            
            BMWAndAudiCars.push(cars[index])
        }
    }
    return BMWAndAudiCars
}

module.exports = getBMWAndAudiCars