const allCarYears = require('./problem4.cjs')
const getOlderCars = (cars) => {
    // console.log(cars)
    let carsYears=allCarYears(cars)
    if ( cars=== undefined ||cars.length==0||typeof(cars)=='string' || typeof(cars)=='number' ) {
        return []
    }
    else if(!Array.isArray(cars)){
        if(carsYears.includes(cars.car_year) && cars.car_year<2000){
            return [cars]
        }
        else{
            return []
        }
    }
    
    // carsYears=[1999]
    // console.log(carsYears)
    const olderCars = []
    for (let index = 0; index < cars.length - 1; index++) {
        if (carsYears.includes(cars[index].car_year) && cars[index].car_year<2000) {
            

            olderCars.push(cars[index])
        }
    }
    return olderCars
}

module.exports = getOlderCars