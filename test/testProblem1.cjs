/*The dealer can't recall the information for a car with an id of 33 on his lot. 
Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, 
and model in the console log in the format of: 
 "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"*/
const inventory = require('../cars.cjs')
// console.log(inventory);
//`Car ${id} is a ${car.car_year} ${car.car_make} ${car.car_model}`
let id = 33
const problem1 = require('../problem1.cjs')

const result = problem1({id: 33, name: "Test", length: 10}, 33)

// console.log(result);

if(!Array.isArray(result)){
console.log(`Car ${result.id} is a ${result.car_year} ${result.car_make} ${result.car_model}`)
}
else{
    console.log([])
}