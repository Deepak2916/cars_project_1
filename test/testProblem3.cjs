/* The marketing team wants the car models listed alphabetically on the website. 
Execute a function to Sort all the car model names into alphabetical order and 
log the results in the console as it was returned.*/


const inventory = require('../cars.cjs')

const problem3 = require('../problem3.cjs')

const result = problem3(inventory)

console.log(result)
 //aacd aad