/* The dealer needs the information on the last car in their inventory. 
Execute a function to find what the make and model of the last car in the inventory is?  
Log the make and model into the console in the format of: 
"Last car is a *car make goes here* *car model goes here*"*/
const inventory = require('../cars.cjs')

const problem2 = require('../problem2.cjs')

const result = problem2({ id: 50, car_make: 'Lincoln', car_model: 'Town Car', car_year: 1999 })

if(!Array.isArray(result)){
    console.log(`"Last car is a ${result.car_make} ${result.car_model}`)
}

